(define-module (r7rs scheme-requests-for-implementation)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module (guix build utils))

(define target-lib "lib/r7rs-libs/")
(define target-srfi (string-append target-lib "srfi/"))

(define-public official-r7rs-srfi-89
  (package
    (name "official-r7rs-srfi-89")
    (version "0.0.1-a5d2155")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/scheme-requests-for-implementation/srfi-89")
               (commit "a5d2155b711b8d1d01e4bca90d741eb232644927")))
        (sha256 (base32 "1kj1sg9jq3svm5hv0670s5fhpsjiaqxzjhacyj0y99iq7jqq2zm5"))))
    (build-system copy-build-system)
    (arguments
     (list
      #:install-plan
      #~`(("contrib/Martin-Becze/srfi/" #$target-srfi))))
    (home-page "https://github.com/scheme-requests-for-implementation/srfi-89")
    (synopsis "Optional positional and named parameters")
    (description "This SRFI specifies the define* and lambda* special forms. These forms extend
the R5RS define and lambda special forms to simplify the use of optional positional and named parameters.")
    (license expat)))

(define-public official-r7rs-srfi-89-no-88
  (package
    (name "official-r7rs-srfi-89-no-88")
    (version "0.0.1-a5d2155")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/scheme-requests-for-implementation/srfi-89")
               (commit "a5d2155b711b8d1d01e4bca90d741eb232644927")))
        (sha256 (base32 "1kj1sg9jq3svm5hv0670s5fhpsjiaqxzjhacyj0y99iq7jqq2zm5"))))
    (build-system copy-build-system)
    (arguments
     (list
      #:install-plan
      #~`(("contrib/Martin-Becze/srfi/" #$target-srfi))
      #:phases
      #~(modify-phases
          %standard-phases
          (add-before
            'install 'patch
            (lambda _
              (substitute*
                "contrib/Martin-Becze/srfi/89.sld"
                (("\\(srfi 88\\)") "")
                (("symbol->string\\)\\)\\)\\)")
                 "symbol->string)))\n   (else))\n")))))))
    (home-page "https://github.com/scheme-requests-for-implementation/srfi-89")
    (synopsis "Optional positional and named parameters without import of srfi 88")
    (description "This package is intended to be used when srfi 88 (keywords) is a built in of 
the implementations (ex: gambit-c).
This SRFI specifies the define* and lambda* special forms. These forms extend
the R5RS define and lambda special forms to simplify the use of optional positional and named parameters.")
    (license expat)))

(define-public official-r7rs-srfi-113
  (package
    (name "official-r7rs-srfi-113")
    (version "0.0.1-1e35180")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/scheme-requests-for-implementation/srfi-113")
               (commit "1e35180f4b224b885a6b4ec51cf76c78ca742658")))
        (sha256 (base32 "0fai1c0fliz998mvahidjha7ia8044352al8mk0cb0n0180h5893"))))
    (build-system copy-build-system)
    (arguments
     (list
      #:install-plan
      #~`(("sets/sets.sld" ,(string-append #$target-srfi "113.sld"))
          ("sets/sets-impl.scm" ,(string-append #$target-srfi "sets-impl.scm")))
      #:phases
      #~(modify-phases
          %standard-phases
          (add-before
            'install 'patch
            (lambda _
              (substitute*
                "sets/sets.sld"
                (("comparators") "srfi 128")
                (("srfi 69") "except (srfi 69) string-hash string-ci-hash")
                (("set-remove set-remove") "set-remove")))))))
    (home-page "https://github.com/scheme-requests-for-implementation/srfi-113")
    (synopsis "Sets and bag")
    (description "Sets and bags (also known as multisets) are unordered collections
that can contain any Scheme object. Sets enforce the constraint that no two elements
can be the same in the sense of the set's associated equality predicate; bags do not.")
    (license expat)))

(define-public official-r7rs-srfi-128
  (package
    (name "official-r7rs-srfi-128")
    (version "0.0.1-c881c9a")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/scheme-requests-for-implementation/srfi-128")
               (commit "c881c9a67a9dd607600cf7bfa768e5753b2cf721")))
        (sha256 (base32 "1zfqljb6d3181kiwfqi92vljzsvvckj1nphhczgkzb9kfh7pxbgh"))))
    (build-system copy-build-system)
    (arguments
     (list
       #:install-plan
       #~`(("srfi/" ,(string-append #$target-srfi)))))
    (home-page "https://github.com/scheme-requests-for-implementation/srfi-128")
    (synopsis "Comparators (reduced)")
    (description "This SRFI provides comparators, which bundle a type test predicate,
an equality predicate, an ordering predicate, and a hash function (the last two are optional)
into a single Scheme object. By packaging these procedures together, they can be treated
as a single item for use in the implementation of data structures.")
    (license expat)))

(define-public official-r7rs-srfi-145
  (package
    (name "official-r7rs-srfi-145")
    (version "0.0.1-7e95c4f3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gitlab.com/ChillPC/srfi-145")
               (commit "7e95c4f3991c886a64039298226a4ce5d8831b1e")))
        (sha256 (base32 "0s6i56ls5a1ppa4fmr0j4fqp3kc9n76q8fl9gr67y5adi4x4168s"))))
    (build-system copy-build-system)
    (arguments
     (list
      #:install-plan
      #~`(("srfi" ,#$target-lib))))
    (home-page "https://gitlab.com/ChillPC/srfi-145")
    (synopsis "Assumptions")
    (description "A means to denote the invalidity of certain code paths in a Scheme program
is proposed. It allows Scheme code to turn the evaluation into a user-defined error that need
not be signalled by the implementation. Optimizing compilers may use these denotations to
produce better code and to issue better warnings about dead code.")
    (license expat)))

(define-public official-r7rs-srfi-146
  (package
    (name "official-r7rs-srfi-146")
    (version "0.0.1-4c63a08")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/scheme-requests-for-implementation/srfi-146")
               (commit "4c63a08d020c855918478e2ca0b15087892f33b6")))
        (sha256 (base32 "0xnv1kv5chwl34rk8r5v7wpahrkzi7s069ywkchx8y87bwhp4178"))))
    (build-system copy-build-system)
    (arguments
     (list
      #:install-plan
      #~`(("srfi" ,#$target-lib)
          ("nieper" ,#$target-lib)
          ("gleckler" ,#$target-lib))
      #:phases
      #~(modify-phases
          %standard-phases
          (add-before
            'install 'patch
            (lambda _
              (substitute*
                "srfi/146.sld"
                (("\\(srfi 1\\)") ""))))))) ; Avoid duplicate import of r7rs identifiers
    (home-page "https://github.com/scheme-requests-for-implementation/srfi-146")
    (synopsis "Mappings")
    (description "Mappings are finite sets of associations, where each association is a pair
consisting of a key and an arbitrary Scheme value. The keys are elements of a suitable domain.
Each mapping holds no more than one association with the same key.
The fundamental mapping operation is retrieving the value of an association stored in the
mapping when the key is given.")
    (license expat)))

(define-public official-r7rs-srfi-197
  (package
    (name "official-r7rs-srfi-197")
    (version "0.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/scheme-requests-for-implementation/srfi-197")
               (commit "d31b8be86460bf837cccf2737a1b9b9c01788573")))
        (sha256 (base32 "1c1jjzqgavjwfzs352wssdbjga5ymv4g3lkl0zxhjw7pfrr5xx1m"))))
    (build-system copy-build-system)
    (arguments
     (list
      #:install-plan
      #~`(("srfi-197.scm" ,(string-append #$target-srfi "197.scm"))
          ("srfi-197.sld" ,(string-append #$target-srfi "197.sld")))
      #:phases
      #~(modify-phases
          %standard-phases
          (add-before
            'install 'patch
            (lambda _
              (substitute*
                "srfi-197.sld"
                (("srfi-197.scm") "197.scm")
                (("srfi-197") "srfi 197")))))))
    (home-page "https://github.com/scheme-requests-for-implementation/srfi-197")
    (synopsis "Pipeline operators")
    (description "Many functional languages provide pipeline operators, like Clojure's ->
or OCaml's |>. Pipelines are a simple, terse, and readable way to write deeply-nested
expressions. This SRFI defines a family of chain and nest pipeline operators, which can
rewrite nested expressions like (a b (c d (e f g))) as a sequence of operations:
(chain g (e f _) (c d _) (a b _)).")
    (license expat)))
