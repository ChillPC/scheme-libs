# Scheme libs guix channel

This channel aims to distribute scheme code to users. Every files are send to your profile at `lib/scheme-libs`

With your env var `GUIX_PROFILE` (usually at `$HOME/.guix-profile/`) if you install it, or `$GUIX_ENVIRONMENt` if you use guix shell (without `guix shell ... -- ...` though), you should launch your scheme implementation like :

- chibi: `chibi-scheme -I $GUIX_ENVIRONMENT/lib/r7rs-libs`
- gambit: `gsi -:~~guixlib=$GUIX_ENVIRONMENT/lib/r7rs-libs,search=~~guixlib`
